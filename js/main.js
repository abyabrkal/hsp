
// Affix + Scrollspy
$('#nav').affix({
    offset: {     
      top: $('#nav').offset().top,
      bottom: ($('footer').outerHeight(true) + $('.application').outerHeight(true)) + 40
    }
});
// End of Affix + Scrollspy